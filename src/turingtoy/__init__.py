from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)

# Function to check if the current state is a final state
def is_final_state(state: str, final_states: List[str]) -> bool:
    return state in final_states

# Function to check if the maximum number of steps has been reached
def max_steps_reached(steps: Optional[int], step_count: int) -> bool:
    return steps is not None and step_count >= steps

# Function to generate output by removing leading and trailing blanks
def generate_output(tape: List[str], blank: str) -> str:
    return ''.join(tape).strip(blank)

# Function to handle tape head movement and tape expansion
def move_head(direction: str, head: int, tape: List[str], blank: str) -> int:
    if direction == "L":
        head -= 1
        if head < 0:
            head = 0
            tape.insert(0, blank)
    elif direction == "R":
        head += 1
        if head == len(tape):
            tape.append(blank)
    return head

# Function to write on the tape and move the tape head
def write_and_move(action: Dict, tape: List[str], head: int, blank: str) -> Tuple[int, str]:
    if "write" in action:
        tape[head] = action['write']

    direction = 'L' if 'L' in action else 'R'
    next_state = action.get(direction)
    head = move_head(direction, head, tape, blank)

    return head, next_state

# Main function to execute a Turing machine transition
def execute_transition(
    machine: Dict,
    tape: List[str],
    head: int,
    state: str,
    steps: Optional[int],
    step_count: int,
    history: List[Dict]
) -> Tuple[str, List[Dict], bool]:

    blank = machine["blank"]
    final_states = machine["final states"]
    transitions = machine["table"]

    if is_final_state(state, final_states):
        output = generate_output(tape, blank)
        return output, history, True

    if max_steps_reached(steps, step_count):
        output = generate_output(tape, blank)
        return output, history, False

    if head < 0 or head >= len(tape):
        tape.insert(0, blank) if head < 0 else tape.append(blank)
        head = max(0, head)

    current_symbol = tape[head]
    if current_symbol not in transitions[state]:
        output = generate_output(tape, blank)
        return output, history, False

    action = transitions[state][current_symbol]
    history.append({
        'state': state,
        'reading': current_symbol,
        'position': head,
        'memory': ''.join(tape),
        'transition': action
    })

    if isinstance(action, str):
        head = move_head(action, head, tape, blank)
    else:
        head, state = write_and_move(action, tape, head, blank)

    return execute_transition(machine, tape, head, state, steps, step_count + 1, history)

# Function to start the Turing machine with a given input
def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List[Dict], bool]:
    tape = list(input_)
    head = 0
    state = machine["start state"]
    history = []

    return execute_transition(machine, tape, head, state, steps, 0, history)